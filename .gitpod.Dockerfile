FROM gitpod/workspace-dotnet-vnc

USER root

RUN apt-get update && apt-mark hold iptables && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
dbus-x11 \
psmisc \
xdg-utils \
x11-xserver-utils \
x11-utils && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
xfce4 && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
libgtk-3-bin \
libpulse0 \
mousepad \
xfce4-notifyd \
xfce4-taskmanager \
xfce4-terminal && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
xfce4-battery-plugin \
xfce4-clipman-plugin \
xfce4-cpufreq-plugin \
xfce4-cpugraph-plugin \
xfce4-diskperf-plugin \
xfce4-datetime-plugin \
xfce4-fsguard-plugin \
xfce4-genmon-plugin \
xfce4-indicator-plugin \
xfce4-netload-plugin \
xfce4-notes-plugin \
xfce4-places-plugin \
xfce4-sensors-plugin \
xfce4-smartbookmark-plugin \
xfce4-systemload-plugin \
xfce4-timer-plugin \
xfce4-verve-plugin \
xfce4-weather-plugin \
xfce4-whiskermenu-plugin && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
libxv1 \
mesa-utils \
mesa-utils-extra && \
sed -i 's%<property name="ThemeName" type="string" value="Xfce"/>%<property name="ThemeName" type="string" value="Raleigh"/>%' /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml 

RUN env DEBIAN_FRONTEND=noninteractive dpkg --add-architecture i386 && \
apt-get update && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y \
fonts-wine \
locales \
ttf-mscorefonts-installer \
wget \
winbind \
winetricks && \
mkdir -p /usr/share/wine/gecko && \
cd /usr/share/wine/gecko && \
wget https://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86.msi && \
wget https://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86_64.msi && \
mkdir -p /usr/share/wine/mono && \
cd /usr/share/wine/mono && \
wget https://dl.winehq.org/wine/wine-mono/4.7.1/wine-mono-4.7.1.msi && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y \
gettext \
gnome-icon-theme \
playonlinux \
q4wine \
xterm && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y \ 
libxv1 \
mesa-utils \
mesa-utils-extra && \
env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
libpulse0 \
pavucontrol \
pasystray \
zenity

RUN env DEBIAN_FRONTEND=noninteractive apt-get install -y \
xfwm4 && \
mkdir -p /etc/skel/.config/lxsession/LXDE && \
echo '[Session]\n\window_manager=xfwm4\n\' >/etc/skel/.config/lxsession/LXDE/desktop.conf 

# Enable this for chinese, japanese and korean fonts in wine
#RUN winetricks -q cjkfonts 
# create desktop icons
#
RUN mkdir -p /etc/skel/Desktop && \
echo '#! /bin/bash \n\
datei="/etc/skel/Desktop/$(echo "$1" | LC_ALL=C sed -e "s/[^a-zA-Z0-9,.-]/_/g" ).desktop" \n\
echo "[Desktop Entry]\n\
Version=1.0\n\
Type=Application\n\
Name=$1\n\
Exec=$2\n\
Icon=$3\n\
" > $datei \n\
chmod +x $datei \n\
n' > /usr/local/bin/createicon && chmod +x /usr/local/bin/createicon 

RUN env DEBIAN_FRONTEND=noninteractive apt-get install -y ubuntu-desktop xrdp synaptic 

RUN apt install gnupg ca-certificates
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN echo "deb https://download.mono-project.com/repo/ubuntu stable-focal main" | tee /etc/apt/sources.list.d/mono-official-stable.list
RUN wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg && \ 
install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/ && \
sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
RUN add-apt-repository ppa:oibaf/graphics-drivers
RUN add-apt-repository ppa:maarten-fonville/android-studio
RUN apt update

RUN apt install mono-complete nuget code code-insiders flatpak gnome-software-plugin-flatpak -y

RUN apt install python3 python3-pip  python3-cryptography -y

RUN apt install android-studio -y

RUN dbus-uuidgen > /var/lib/dbus/machine-id

RUN mkdir -p /var/run/dbus && dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address

RUN apt upgrade -y

ENV container docker

RUN apt-get update && \
 DEBIAN_FRONTEND=noninteractive \
 apt-get install -y fuse snapd fuse snap-confine squashfuse sudo init && \
 apt-get clean && \
 dpkg-divert --local --rename --add /sbin/udevadm && \
 ln -s /bin/true /sbin/udevadm
 
#ADD snap /usr/local/bin/snap
ENV PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:$PATH"
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN systemctl enable snapd
VOLUME ["/sys/fs/cgroup"]

#RUN /sbin/init

USER gitpod

RUN pip3 install projector-installer --user
RUN sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#RUN sudo flatpak install flathub org.desmume.DeSmuME -y
#RUN sudo flatpak install flathub io.mrarm.mcpelauncher -y
#RUN sudo flatpak install flathub com.mojang.Minecraft -y
#RUN sudo flatpak install flathub com.wps.Office -y
#RUN sudo flatpak install flathub org.onlyoffice.desktopeditors -y
#RUN sudo flatpak install flathub com.google.AndroidStudio -y
#RUN sudo flatpak install flathub io.dbeaver.DBeaverCommunity -y
#RUN sudo flatpak install flathub com.github.muriloventuroso.easyssh -y
#RUN sudo flatpak install flathub org.apache.netbeans -y
#RUN sudo flatpak install flathub com.getpostman.Postman -y
#RUN sudo flatpak install flathub com.dosbox_x.DOSBox-X -y

#RUN /usr/local/bin/createicon "PlayOnLinux" "playonlinux" playonlinux && \
#/usr/local/bin/createicon "Q4wine" "q4wine" q4wine && \
#/usr/local/bin/createicon "Internet Explorer" "wine iexplore" applications-internet && \
#/usr/local/bin/createicon "Console" "wineconsole" utilities-terminal && \
#/usr/local/bin/createicon "File Explorer" "wine explorer" folder && \
#/usr/local/bin/createicon "Notepad" "wine notepad" wine-notepad && \
#/usr/local/bin/createicon "Wordpad" "wine wordpad" accessories-text-editor && \
#/usr/local/bin/createicon "winecfg" "winecfg" wine-winecfg && \
#/usr/local/bin/createicon "WineFile" "winefile" folder-wine && \
#/usr/local/bin/createicon "Mines" "wine winemine" face-cool && \
#/usr/local/bin/createicon "winetricks" "winetricks -gui" wine && \
#/usr/local/bin/createicon "Registry Editor" "regedit" preferences-system && \
#/usr/local/bin/createicon "UnInstaller" "wine uninstaller" wine-uninstaller && \
#/usr/local/bin/createicon "Taskmanager" "wine taskmgr" utilities-system-monitor && \
#/usr/local/bin/createicon "Control Panel" "wine control" preferences-system && \
#/usr/local/bin/createicon "OleView" "wine oleview" preferences-system && \
#/usr/local/bin/createicon "CJK fonts installer chinese japanese korean" "xterm -e \"winetricks cjkfonts\"" font

CMD ["startxfce4"]